import React from 'react';
import { StyleSheet, View, StatusBar } from 'react-native';
import { Container, Header, Left, Body, Right, Title, Form, Item, Label, Input, Button, Text, StyleProvider } from 'native-base';
import Login from './components/Login'

// At the top of your file
import * as Font from 'expo-font'
import { Ionicons } from '@expo/vector-icons';

import { YellowBox } from 'react-native';

export default class App extends React.Component {

  constructor() {
    super();
    YellowBox.ignoreWarnings(['Setting a timer']);
    this.state = {
      fontLoaded: false,
      logged: false
    }
  }

  async componentDidMount() {
    await Font.loadAsync({
      'Roboto': require('native-base/Fonts/Roboto.ttf'),
      'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
      ...Ionicons.font,
    });
    this.setState({
      fontLoaded: true
    })
  }

  render() {
    let view = (
      <View style={{ marginTop: StatusBar.currentHeight }} >
        <Header>
          <Left style={{ flex: 1, marginLeft: 20 }}><Title>Home</Title></Left>
          <Body />
          <Right />
        </Header>
      </View>)
    return (
      this.state.fontLoaded ? (
        <View style={styles.container}>
          {this.state.logged ? view : <Login onLog={() => this.setState({ logged: true })} />}
        </View>) : null
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000000',
  },
});
