import React from 'react';
import { StyleSheet, View, StatusBar, DrawerLayoutAndroid, Image, BackHandler } from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text, Form, Item, Input, Label, Toast } from 'native-base';
import { ProgressDialog } from 'react-native-simple-dialogs';
import ControlPanel from './ControlPanel'
import Film from './Film.js'
import SerieTV from './SerieTV.js'
import Videogiochi from './Videogiochi.js'
import Carrello from './Carrello.js'
import Wishlist from './Wishlist.js'
import SnackBar from 'react-native-snackbar-component'

export default class HomeScreen extends React.Component {

    constructor() {
        super();
        this.state = {
            loggedIn: false,
            dialogVisible: false,
            categoria: 'Home',
            welcomeSnackBarVisible: true,
        }

        this.handleGoBack = this.handleGoBack.bind(this);
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleGoBack);
    }

    handleGoBack() {
        if (this.state.categoria != 'Home') {
            this.setState({ categoria: 'Home' })
            return true;
        }
        return false;
    }

    removeSnackBar() {
        this.setState({ welcomeSnackBarVisible: false })
    }

    render() {

        setTimeout(() => { this.setState({ welcomeSnackBarVisible: false }) }, 3000)

        var categoria;
        if (this.state.categoria === 'Film') {
            categoria = <Film />
        } else if (this.state.categoria === 'Serie TV') {
            categoria = <SerieTV />
        } else if (this.state.categoria === 'Videogiochi') {
            categoria = <Videogiochi />
        } else if (this.state.categoria === 'Carrello') {
            categoria = <Carrello />
        } else if (this.state.categoria === 'Wishlist') {
            categoria = <Wishlist />
        } else {
            categoria = (
                <View style={{ flex: 1, justifyContent: 'center' }}>
                    <Text style={{ textAlign: 'center', fontSize: 40, marginBottom: 20 }}>Bentornato, apri il drawer e scegli una categoria!!</Text>
                </View>);
        }

        var navigationView = (
            <View style={{ flex: 1, backgroundColor: '#fff' }}>
                <Image style={{ width: "100%", height: "30%", resizeMode: 'cover' }} source={{ uri: "https://images.unsplash.com/photo-1483705385549-e04ea91e0011?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80" }} />
                <Button style={{ marginTop: 20 }} iconLeft full transparent primary onPress={() => { this.setState({ categoria: 'Home' }); this._drawer.closeDrawer() }}>
                    <Icon name='home' />
                    <Text>Home</Text>
                </Button>
                <Button style={{ marginTop: 20 }} iconLeft full transparent primary onPress={() => { this.setState({ categoria: 'Film' }); this._drawer.closeDrawer() }}>
                    <Icon name='md-film' />
                    <Text>Film</Text>
                </Button>
                <Button style={{ marginTop: 20 }} iconLeft full transparent primary onPress={() => { this.setState({ categoria: 'Serie TV' }); this._drawer.closeDrawer() }}>
                    <Icon name='ios-tv' />
                    <Text>Serie TV</Text>
                </Button>
                <Button style={{ marginTop: 20 }} iconLeft full transparent primary onPress={() => { this.setState({ categoria: 'Videogiochi' }); this._drawer.closeDrawer() }}>
                    <Icon name='logo-game-controller-b' />
                    <Text>Videogiochi</Text>
                </Button>
                <Button style={{ marginTop: 20 }} iconLeft full transparent primary onPress={() => { this.setState({ categoria: 'Carrello' }); this._drawer.closeDrawer() }}>
                    <Icon name='md-cart' />
                    <Text>Carrello</Text>
                </Button>
                <Button style={{ marginTop: 20 }} iconLeft full transparent primary onPress={() => { this.setState({ categoria: 'Wishlist' }); this._drawer.closeDrawer() }}>
                    <Icon name='md-heart' />
                    <Text>Wish List</Text>
                </Button>
            </View>
        );

        return (
            <Container style={{ marginTop: StatusBar.currentHeight }}>
                <DrawerLayoutAndroid
                    drawerWidth={300}
                    drawerPosition={DrawerLayoutAndroid.positions.Left}
                    renderNavigationView={() => navigationView}
                    ref={ref => this._drawer = ref}>
                    <Header>
                        <Left>
                            <Button transparent onPress={() => this._drawer.openDrawer()}>
                                <Icon name='menu' />
                            </Button>
                        </Left>
                        <Body>
                            <Title>{this.state.categoria}</Title>
                        </Body>
                        <Right />
                    </Header>
                    <View style={{ flex: 1 }}>
                        {categoria}
                    </View>
                </DrawerLayoutAndroid>
            </Container>
        );
    }
}

