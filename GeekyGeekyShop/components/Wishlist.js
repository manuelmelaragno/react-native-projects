import React from 'react';
import { StyleSheet, View, StatusBar, ScrollView, TouchableOpacity, Image, FlatList, Text } from 'react-native';
import CardMine from './CardMine.js'
import SnackBar from 'react-native-snackbar-component'

export default class Wishlist extends React.Component {

    constructor() {
        super();
        this.state = {
            data: null
        }

        this.loadFilms = this.loadFilms.bind(this);
        this.handleAggiunta = this.handleAggiunta.bind(this)
    }

    componentDidMount() {
        this.loadFilms();
    }

    loadFilms(e) {
        console.log("Carico carrello...");
        this.setState({
            dialogVisible: true,
        })
        fetch('https://geekygeekyshop.000webhostapp.com/wishlist/get_wishlist.php', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                id_utente: '1',
            })
        }).then(response => response.json())
            .then(data => {
                console.log(data)
                console.log("fatto")
                //console.log(data[0].nome)
                if (data != null && data.length > 0) {
                    //console.log(data[0].nome)
                    this.setState({
                        data: data,
                    })
                } else {
                    this.setState({
                        data: null,
                    })
                }
            });
    }

    handleAggiunta(text) {
        this.setState({
            snackBarText: text,
            snackBarVisible: true
        })
        this.loadFilms();
        this.timeout = setTimeout(() => { this.setState({ snackBarVisible: false }) }, 3000)
    }

    componentWillUnmount() {
        clearTimeout(this.timeout);
    }

    render() {
        var view;
        if (this.state.data === null) {
            view = <Text style={{ fontSize: 30, marginTop: 'auto', marginBottom: 'auto', marginLeft: 'auto', marginRight: 'auto' }}>Wishlist vuota!</Text>
        } else if (this.state.data.length > 0) {
            view = <FlatList
                contentContainerStyle={{ flexGrow: 1 }}
                numColumns="2"
                data={this.state.data}
                extraData={this.state.data}
                renderItem={({ item }) => <CardMine handleAggiunta={this.handleAggiunta} id={item.id} titolo={item.nome} imgUri={item.url_img} prezzo={item.prezzo + "€"} tipo="Wishlist" />}
                keyExtractor={(item, index) => index.toString()} />
        }
        return (
            <View style={{ flex: 1, flexDirection: 'row' }}>
                {view}
                <SnackBar visible={this.state.snackBarVisible} textMessage={this.state.snackBarText} actionHandler={() => this.setState({ snackBarVisible: false })} actionText="Capito" />
            </View>
        );
    }
}