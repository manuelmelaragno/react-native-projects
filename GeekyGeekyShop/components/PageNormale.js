import React from 'react';
import { StyleSheet, View, Text, Button } from 'react-native';

export default class PageNormale extends React.Component {

    constructor() {
        super();
    }

    render() {
        return (
            <View>
                <Text>Normale</Text>
                <Button title="Vai Home" onPress={() => this.props.navigation.navigate('Home')} />
            </View>
        )
    }
}