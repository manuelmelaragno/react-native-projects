import React from 'react';
import { StyleSheet, View, StatusBar, ScrollView, Text, TouchableOpacity } from 'react-native';

export default class ControlPanel extends React.Component {

    render() {
        return (
            <ScrollView style={styles.container}>
                <Text style={styles.controlText}>Control Panel</Text>
                <TouchableOpacity style={styles.button} onPress={this.props.closeDrawer}>
                    <Text style={{ color: 'white' }}>Close Drawer</Text>
                </TouchableOpacity>
            </ScrollView>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: StatusBar.currentHeight,
        backgroundColor: 'white',
    },
    controlText: {
        color: 'black',
    },
    button: {
        backgroundColor: 'black',
        borderWidth: 1,
        borderColor: 'black',
        padding: 10,
    }
})