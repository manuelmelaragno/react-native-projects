import React from 'react';
import { StyleSheet, View, StatusBar, AsyncStorage } from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text, Form, Item, Input, Label, Toast } from 'native-base';
import { ProgressDialog } from 'react-native-simple-dialogs';
import { TextField } from 'react-native-material-textfield';

export default class LoginPage extends React.Component {

    constructor() {
        super();
        this.state = {
            fontLoaded: false,
            username: "",
            password: "",
            loggedIn: false,
            dialogVisible: false,
        }

        this.handleLogInClick = this.handleLogInClick.bind(this)
    }

    handleLogInClick(e) {
        console.log(this.state.username + " : " + this.state.password);
        this.setState({
            dialogVisible: true,
        })
        fetch('https://geekygeekyshop.000webhostapp.com/utenti.php', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                metodo: 'login',
                emailLogin: this.state.username,
                passwordLogin: this.state.password
            })
        }).then(response => response.json())
            .then(data => {
                console.log(data)
                if (data.status === "successo") {

                    this.setState({ loggedIn: true }, () => { this.setState({ dialogVisible: false }) })
                    this.props.onLog();
                } else {
                    this.setState({ loggedIn: false }, () => { this.setState({ dialogVisible: false }) })
                }
            });
    }

    render() {
        return (
            <Container style={{ marginTop: StatusBar.currentHeight, }}>
                <Header noLeft>
                    <Left>
                    </Left>
                    <Body>
                        <Title>Geeky Geeky Shop</Title>
                    </Body>
                    <Right />
                </Header>
                <Content style={{ padding: "2%" }}>
                    <Form>
                        <TextField
                            label='Email'
                            value={this.state.username}
                            onChangeText={(text) => this.setState({ username: text })}
                            textContentType={'emailAddress'}
                        />
                        <TextField
                            label='Password'
                            value={this.state.password}
                            onChangeText={(text) => this.setState({ password: text })}
                            secureTextEntry={true}
                            textContentType={'password'}
                        />
                        <Button bordered style={{ marginTop: 20, marginLeft: "auto" }} onPress={this.handleLogInClick}>
                            <Text>Log In</Text>
                        </Button>
                    </Form>
                    <ProgressDialog
                        visible={this.state.dialogVisible}
                        title="Sto loggando.."
                        message="Attendere..."
                    />
                </Content>
            </Container>

        );
    }
}
