import React from 'react';

// At the top of your file
import { Font } from 'expo';
import { Ionicons } from '@expo/vector-icons';
import {
    StyleSheet,
    View,
    Image,
    TextInput,
} from 'react-native';
import { List, ListItem, Thumbnail, Text, Left, Body, Right, Button } from 'native-base';

export default class MeteoCurrent extends React.Component {
    constructor() {
        super();
        this.state = {
        }
    }

    componentWillMount() {
        fetch("https://api.openweathermap.org/data/2.5/weather?q=" + this.props.citta + ",it&appid=f887ba25f35c860424a51e119b9d4178&lang=it&units=metric")
            .then(response => response.json())
            .then(data => {
                console.log(data.cod)
                if (data.cod == 200) {
                    this.setState({
                        meteoData: data
                    })
                }

                console.log(data)
            });
    }

    render() {

        let imgSrc = this.state.meteoData ? "https://openweathermap.org/img/w/" + this.state.meteoData.weather[0].icon + ".png" : null;
        let situazione = this.state.meteoData ? "Situazione attuale " + this.state.meteoData.weather[0].description : null;
        let temp = this.state.meteoData ? "Temp. : " + this.state.meteoData.main.temp + "°\nTemp. min : "
            + this.state.meteoData.main.temp_min + "°\nTemp. max : " + this.state.meteoData.main.temp_max : null;

        return (
            <View style={styles.container} >
                <List>
                    <ListItem thumbnail>
                        <Left>
                            < Thumbnail square source={{ uri: imgSrc }} />
                        </Left>
                        <Body>
                            <Text>{situazione}</Text>
                            <Text note numberOfLines={5}>{temp}</Text>
                        </Body>
                        <Right>
                        </Right>
                    </ListItem>
                </List>
            </View >
        );
    }

}

const styles = StyleSheet.create({
    container: {
        width: "100%"
    },
    situazione: {
        fontSize: 20,
        fontWeight: 'bold',
    },
});