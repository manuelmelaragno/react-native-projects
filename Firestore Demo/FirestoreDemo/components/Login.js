import React from 'react';
import { StyleSheet, View, DrawerLayoutAndroid, StatusBar } from 'react-native';
import { Container, Header, Left, Body, Right, Title, Form, Item, Label, Input, Button, Text, StyleProvider } from 'native-base';
import db from "../firebase/firebase.js"

export default class Login extends React.Component {

    constructor() {
        super();

        this.state = {
            email: "",
            password: ""
        }

        this.handleLogInButton = this.handleLogInButton.bind(this);
    }

    handleLogInButton(e) {
        console.log("Log In");

        //Prendo la raccolta dal db
        db.collection("utenti")
            .where("email", "==", this.state.email) //Eseguo la query, con operatore AND
            .where("password", "==", this.state.password)
            .get()
            .then(querySnapshot => {
                const data = querySnapshot.docs.map(doc => doc.data());
                if (data.length > 0) {
                    console.log("Riuscito")
                    this.props.onLog();
                } else {
                    console.log("Non riuscito")
                }
            });

    }

    render() {
        return (
            <View style={{ marginTop: StatusBar.currentHeight }} >
                <Header>
                    <Left style={{ flex: 1, marginLeft: 20 }}><Title>Login</Title></Left>
                    <Body />
                    <Right />
                </Header>
                <View style={{ height: '100%', backgroundColor: '#fff', paddingTop: 50, paddingRight: 25, paddingLeft: 25 }}>
                    <Form>
                        <Item floatingLabel>
                            <Label>Username</Label>
                            <Input onChangeText={(text) => this.setState({ email: text })} />
                        </Item>

                        <Item floatingLabel style={{ marginTop: 50 }}>
                            <Label>Password</Label>
                            <Input onChangeText={(text) => this.setState({ password: text })} secureTextEntry={true} />
                        </Item>
                        <Button primary onPress={this.handleLogInButton} style={{ width: 100, marginLeft: 'auto', marginRight: 0, marginTop: 30, justifyContent: 'center' }}><Text> Login </Text></Button>
                    </Form>
                </View>
            </ View>
        );
    }
}
