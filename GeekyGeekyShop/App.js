import React from 'react';
import { StyleSheet, View, Platform } from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text, Form, Item, Input, Label, Toast } from 'native-base';
import { ProgressDialog } from 'react-native-simple-dialogs';
import LoginPage from './components/LoginPage'
import HomeScreen from './components/HomeScreen'

// At the top of your file
import * as Font from 'expo-font'
import { Ionicons } from '@expo/vector-icons';

export default class App extends React.Component {

  constructor() {
    super();
    this.state = {
      fontLoaded: false,
      logged: true //cambiare in false
    }
    this.logHandler = this.logHandler.bind(this);
  }

  async componentDidMount() {
    await Font.loadAsync({
      'Roboto': require('native-base/Fonts/Roboto.ttf'),
      'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
      ...Ionicons.font,
    });
    this.setState({
      fontLoaded: true
    })
  }

  logHandler() {
    this.setState({
      logged: true
    }, () => { console.log(this.state.logged) })
  }

  render() {
    return (
      this.state.fontLoaded ? (
        <View style={styles.container}>
          {this.state.logged ? <HomeScreen /> : <LoginPage onLog={this.logHandler} />}
        </View>
      ) : null
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "black"
  }
});