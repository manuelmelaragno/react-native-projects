import * as firebase from 'firebase';
import 'firebase/firestore';

//Configurazione per FireBase
const firebaseApp = firebase.initializeApp({
    // copy and paste your firebase credential here
    apiKey: "AIzaSyC4-7Ww3LTCqoZMCXhLWjbFN86bsva_AfY",
    authDomain: "nitmy-d915c.firebaseapp.com",
    databaseURL: "https://nitmy-d915c.firebaseio.com",
    projectId: "nitmy-d915c",
    storageBucket: "nitmy-d915c.appspot.com",
    messagingSenderId: "270651032806",
    appId: "1:270651032806:web:0485f91e077c53ba12e727",
    measurementId: "G-M894K43YBP"
});

const db = firebaseApp.firestore();

export default db;
