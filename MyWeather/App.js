import React from 'react';
import MeteoCurrent from './components/MeteoCurrent.js'

// At the top of your file
import { Font } from 'expo';
import { Ionicons } from '@expo/vector-icons';
import {
  StyleSheet,
  View,
  Image,
  TextInput,
} from 'react-native';
import { Container, Header, Content, Button, Text, Icon, Item, Input, Left, Right, Body, Title } from 'native-base';


export default class App extends React.Component {


  constructor() {
    super();
    this.state = {
      fontLoaded: false,
      città: "",
      meteoCurrentMount: false,
      headerText: "Meteo"
    }
    this.buttonPressed = this.buttonPressed.bind(this);
  }

  buttonPressed(target) {
    if (this.state.città != "") {
      this.setState({
        meteoCurrentMount: true,
        headerText: "Meteo " + this.state.città
      }, () => {
        console.log(this.state.meteoCurrentMount);
        console.log(this.state.città)
      })
    }
  }

  render() {

    let meteoCurrent;
    meteoCurrent = this.state.meteoCurrentMount ? <MeteoCurrent citta={this.state.città} /> : null;

    return (
      this.state.fontLoaded ? (
        < Container style={{ marginTop: "10%" }}>
          <Header noLeft >
            <Left>
              <Button transparent>
                <Icon name="arrow-back" />
              </Button>
            </Left>
            <Body>
              <Title>{this.state.headerText}</Title>
            </Body>
            <Right>
            </Right>
          </Header>
          <View style={styles.container}>
            <Item>
              <Icon active name='home' />
              <Input placeholder='Inserisci città' onChangeText={(città) => this.setState({ città })} value={this.state.text} />
            </Item>
            <View style={styles.button}>
              <Button iconLeft primary onPress={this.buttonPressed}>
                <Icon name='search' />
                <Text>Cerca</Text>
              </Button>
            </View>
          </View>
          {meteoCurrent}
        </Container>) : null
    );
  }


  async componentDidMount() {
    await Font.loadAsync({
      'Roboto': require('native-base/Fonts/Roboto.ttf'),
      'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),
    });

    this.setState({
      fontLoaded: true
    });
  }
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: "20%",
    backgroundColor: '#ffffff',
    alignItems: 'center',
    paddingLeft: "10%",
    paddingRight: "10%",
    paddingTop: "5%",
  },
  image: {
    width: "100%",
    height: "30%",
    resizeMode: 'contain',
  },
  textInput: {
    height: 50,
    width: "100%",
    marginTop: "5%",
    borderColor: "black",
    borderBottomWidth: 2,
    fontSize: 20,
    color: "black",
    marginTop: "10%",
  },
  button: {
    flex: 1,
    marginTop: "5%",
    marginLeft: 'auto',
  }
});
