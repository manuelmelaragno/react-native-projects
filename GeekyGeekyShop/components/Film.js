import React from 'react';
import { StyleSheet, View, StatusBar, ScrollView, TouchableOpacity, Image, FlatList, AsyncStorage } from 'react-native';
import CardMine from './CardMine.js'
import SnackBar from 'react-native-snackbar-component'

export default class Film extends React.Component {

    constructor() {
        super();
        this.state = {
            data: null,
            snackBarVisible: false,
            snackBarText: ""
        }

        this.loadFilms = this.loadFilms.bind(this);
        this.handleAggiunta = this.handleAggiunta.bind(this);

    }

    componentDidMount() {
        this.loadFilms();
    }

    loadFilms(e) {
        console.log("Carico films...");
        this.setState({
            dialogVisible: true,
        })
        fetch('https://geekygeekyshop.000webhostapp.com/get_prodotti.php', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                tipo: 'film',
            })
        }).then(response => response.json())
            .then(data => {
                console.log(data)
                console.log("fatto")
                console.log(data[0].url_img)
                //console.log(data[0].nome)
                this.setState({
                    data: data,
                })
            });
    }

    handleAggiunta(text) {
        this.setState({
            snackBarText: text,
            snackBarVisible: true
        })
        this.timeout = setTimeout(() => { this.setState({ snackBarVisible: false }) }, 3000)
    }

    componentWillUnmount() {
        clearTimeout(this.timeout);
    }

    render() {
        return (
            <View style={{ flex: 1, flexDirection: 'row' }}>
                <FlatList
                    contentContainerStyle={{ flexGrow: 1 }}
                    numColumns="2"
                    data={this.state.data}
                    extraData={this.state.data}
                    renderItem={({ item }) => <CardMine handleAggiunta={this.handleAggiunta} id={item.id} titolo={item.nome} imgUri={item.url_img} prezzo={item.prezzo + "€"} tipo="Prodotto" />}
                    keyExtractor={(item, index) => index.toString()} />
                <SnackBar visible={this.state.snackBarVisible} textMessage={this.state.snackBarText} actionHandler={() => this.setState({ snackBarVisible: false })} actionText="Capito" />
            </View>
        );
    }
}