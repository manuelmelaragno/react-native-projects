import React from 'react';
import { StyleSheet, View, StatusBar, ScrollView, TouchableOpacity, Image, Dimensions } from 'react-native';
import { Container, Header, Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body } from 'native-base';
import SnackBar from 'react-native-snackbar-component'

export default class CardMine extends React.Component {

    constructor() {//Bisogna passare alle props il titolo, la descrizione, la foto
        super();

        this.state = {
            snackBarVisible: false,
            snackBarText: ""
        }

        this.handleCarrello = this.handleCarrello.bind(this);
        this.handleWishList = this.handleWishList.bind(this);
        this.handleRimuoviCarrello = this.handleRimuoviCarrello.bind(this);
        this.handleRimuoviWishlist = this.handleRimuoviWishlist.bind(this);
    }

    handleWishList() {
        console.log("Aggiungo alla wishList " + this.props.titolo + " ID : " + this.props.id + " : " + this.props.tipo);
        fetch('https://geekygeekyshop.000webhostapp.com/wishlist/agg_wishlist.php', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                id_utente: '1',
                idProdotto: this.props.id
            })
        }).then(response => response.json())
            .then(data => {
                console.log(data)
                console.log("fatto")
                if (data.status === "successo") {
                    this.props.handleAggiunta("Prodotto aggiunto alla wishlist!")
                } else if (data.status === "errore, gia presente") {
                    this.props.handleAggiunta("Prodotto già nella wishlist!")
                }
            });
    }

    handleCarrello() {
        console.log("Aggiungo al carrello" + this.props.titolo + " ID : " + this.props.id + " : " + this.props.carrello);
        fetch('https://geekygeekyshop.000webhostapp.com/carrello/agg_carrello.php', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                id_utente: '1',
                idProdotto: this.props.id
            })
        }).then(response => response.json())
            .then(data => {
                console.log(data)
                console.log("fatto")
                if (data.status === "successo") {
                    this.props.handleAggiunta("Prodotto aggiunto al carrello!")
                } else if (data.status === "errore, gia presente") {
                    this.props.handleAggiunta("Prodotto già nel carrello!")
                }
            });
    }

    handleRimuoviCarrello() {
        console.log("Rimuovo : " + this.props.titolo);
        fetch('https://geekygeekyshop.000webhostapp.com/carrello/rim_carrello.php', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                id_utente: '1',
                idProdotto: this.props.id
            })
        }).then(response => response.json())
            .then(data => {
                console.log(data)
                console.log("fatto")
                if (data.status === "successo") {
                    this.props.handleAggiunta("Prodotto rimosso dal carrello!")
                } else if (data.status === "errore, gia presente") {
                    this.props.handleAggiunta("Errore!")
                }
            });
    }

    handleRimuoviWishlist() {
        console.log("Rimuovo : " + this.props.titolo);
        fetch('https://geekygeekyshop.000webhostapp.com/wishlist/rim_wishlist.php', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                id_utente: '1',
                idProdotto: this.props.id
            })
        }).then(response => response.json())
            .then(data => {
                console.log(data)
                console.log("fatto")
                if (data.status === "successo") {
                    this.props.handleAggiunta("Prodotto rimosso dalla wishlist!")
                } else if (data.status === "errore, gia presente") {
                    this.props.handleAggiunta("Errore!")
                }
            });
    }

    render() {

        var imgUrl1 = this.props.imgUri.substring(1);
        var imgUrl = imgUrl1.substring(1);
        return (
            <View style={{ flex: 1, margin: 5, backgroundColor: '#fcfcfc', elevation: 10 }}>
                <Image style={{ flex: 1, height: 300, resizeMode: 'cover' }} source={{ uri: 'https://geekygeekyshop.000webhostapp.com/' + imgUrl }} />
                <Text style={{ flexWrap: 'wrap', fontSize: 22, margin: 10 }} numberOfLines={1}>{this.props.titolo}</Text>
                <Text style={{ fontSize: 16, margin: 10 }} numberOfLines={3}>Prezzo {this.props.prezzo}</Text>
                {this.props.tipo === "Carrello" || this.props.tipo === "Wishlist" ? (<Button transparent primary style={{ marginLeft: 'auto' }} onPress={this.props.tipo === "Carrello" ? this.handleRimuoviCarrello : this.handleRimuoviWishlist}><Text>Rimuovi</Text></Button>) : (
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <Button transparent primary style={{ marginLeft: 'auto' }} onPress={this.handleWishList}>
                            <Icon name='md-heart' />
                        </Button>
                        <Button transparent primary style={{ marginLeft: 0 }} onPress={this.handleCarrello}>
                            <Icon name='md-cart' />
                        </Button>
                    </View>)}
            </View>
        );
    }
}